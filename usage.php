<?php

$api = new AkenoApi(config('akeneo.connections.rest_api.endpoint'), config('akeneo.authentication'));
try {
    $data = $api->getPaginated('/products', [
        'pagination_type' => 'search_after',
        'limit' => 100
    ]);
} catch (AkenoApiException $e) {
    // Handle error
    dump($e->getMessage());
}